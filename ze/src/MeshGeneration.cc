#include "MeshGeneration.h"

Mesh generateCube(ID3D11Device* device, float3 extents) {
	Mesh ret{};
	BreakingHResult hr{};

	struct Vertex {
		float3 position;
		float3 tangent;
		float3 bitangent;
		float3 normal;
		float2 texCoord;
	};

	AABB box;
	box.SetFromCenterAndSize(float3::zero, extents);
	float3 corners[8];
	box.GetCornerPoints(corners);
	Vertex vb[24]{};

	float3 normals[] = {
		-float3::unitX,
		+float3::unitX,
		-float3::unitY,
		+float3::unitY,
		-float3::unitZ,
		+float3::unitZ,
	};

	uint32_t srcIndices[] = {
		1, 3, 0, 0, 3, 2,
		4, 6, 5, 5, 6, 7,
		1, 0, 5, 5, 0, 4,
		7, 6, 3, 3, 6, 2,
		2, 6, 0, 0, 6, 4,
		7, 3, 5, 5, 3, 1,
	};

	float3 tangent = float3::unitX;
	float3 bitangent = float3::unitY;
	float2 texCoord = float2::zero;

	uint32_t dstIndices[36]{};
	for (int face = 0; face < 6; ++face) {
		auto vOff = face*4;
		auto* src = srcIndices + face*6;
		auto* dst = dstIndices + face*6;
		auto* v = vb + vOff;
		v[0] = { corners[src[0]], tangent, bitangent, normals[face], texCoord };
		v[1] = { corners[src[1]], tangent, bitangent, normals[face], texCoord };
		v[2] = { corners[src[2]], tangent, bitangent, normals[face], texCoord };
		v[3] = { corners[src[5]], tangent, bitangent, normals[face], texCoord };
		dst[0] = vOff+0;
		dst[1] = vOff+1;
		dst[2] = vOff+2;
		dst[3] = vOff+2;
		dst[4] = vOff+1;
		dst[5] = vOff+3;
	}

	ret.vertexCount = sizeof(vb)/sizeof(vb[0]);
	D3D11_BUFFER_DESC vbDesc{};
	vbDesc.ByteWidth = sizeof(vb);
	vbDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vbDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	vbDesc.CPUAccessFlags = 0;
	vbDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	vbDesc.StructureByteStride = sizeof(Vertex);
	D3D11_SUBRESOURCE_DATA vbSRD{};
	vbSRD.pSysMem = vb;
	hr = device->CreateBuffer(&vbDesc, &vbSRD, &ret.vertexBuffer);
	hr = device->CreateShaderResourceView(ret.vertexBuffer, nullptr, &ret.vertexBufferSRV);

	ret.indexCount = sizeof(dstIndices)/sizeof(dstIndices[0]);
	D3D11_BUFFER_DESC ibDesc{};
	ibDesc.ByteWidth = sizeof(dstIndices);
	ibDesc.Usage = D3D11_USAGE_IMMUTABLE;
	ibDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	ibDesc.CPUAccessFlags = 0;
	ibDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	ibDesc.StructureByteStride = sizeof(uint32_t);
	D3D11_SUBRESOURCE_DATA ibSRD{};
	ibSRD.pSysMem = dstIndices;
	hr = device->CreateBuffer(&ibDesc, &ibSRD, &ret.indexBuffer);
	hr = device->CreateShaderResourceView(ret.indexBuffer, nullptr, &ret.indexBufferSRV);

	return ret;
}