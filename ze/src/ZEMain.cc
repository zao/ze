#include <Windows.h>
#include <Shlwapi.h>

#include <array>
#include <functional>
#include <memory>
#include <random>
#include <vector>

#include "MeshGeneration.h"

#include "DirectXTypes.h"
#include <MathGeoLib/MathGeoLib.h>

struct Graphics {
	explicit Graphics(HWND wnd);
	D3D_FEATURE_LEVEL featureLevel;
	IDXGISwapChainPtr swapChain;
	ID3D11DevicePtr device;
	ID3D11DeviceContextPtr context;
	ID3D11Texture2DPtr backBuffer;
	ID3D11RenderTargetViewPtr backBufferRTV;
};

Graphics::Graphics(HWND wnd) {
	BreakingHResult hr = S_OK;
	hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_DEBUG | D3D11_CREATE_DEVICE_BGRA_SUPPORT,
		nullptr, 0, D3D11_SDK_VERSION, &device, &featureLevel, &context);

	auto pixelFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
	UINT bestCount{1}, bestQuality{0};
	for (UINT count = 1; count <= D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; ++count) {
		UINT quality{};
		hr = device->CheckMultisampleQualityLevels(pixelFormat, count, &quality);
		if (bestCount < count && quality > 0) {
			bestCount = count;
			bestQuality = quality - 1;
		}
	}
	RECT clientRect{};
	GetClientRect(wnd, &clientRect);
	int width = clientRect.right, height = clientRect.bottom;
	DXGI_SWAP_CHAIN_DESC scd{};
	scd.BufferDesc.Width = width;
	scd.BufferDesc.Height = height;
	scd.BufferDesc.Format = pixelFormat;
	scd.SampleDesc.Count = bestCount;
	scd.SampleDesc.Quality = bestQuality;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.BufferCount = 2;
	scd.OutputWindow = wnd;
	scd.Windowed = TRUE;
	scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scd.Flags = 0;
	IDXGIDevicePtr dxgiDevice;
	hr = device->QueryInterface(&dxgiDevice);
	IDXGIAdapterPtr dxgiAdapter;
	hr = dxgiDevice->GetAdapter(&dxgiAdapter);
	IDXGIFactoryPtr dxgiFactory;
	hr = dxgiAdapter->GetParent(dxgiFactory.GetIID(), reinterpret_cast<void**>(&dxgiFactory));
	hr = dxgiFactory->CreateSwapChain(dxgiDevice, &scd, &swapChain);
	hr = swapChain->GetBuffer(0, backBuffer.GetIID(), reinterpret_cast<void**>(&backBuffer));
	hr = device->CreateRenderTargetView(backBuffer, nullptr, &backBufferRTV);
}

struct Renderer {
	Renderer(ID3D11DevicePtr device, ID3D11DeviceContextPtr context, std::wstring sitedir) {
	}
};

std::function<void()>* onPaint;
std::function<void()>* onResize;
std::function<void()>* onDestroy;

LRESULT CALLBACK WindowProc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	switch (msg) {
	case WM_KEYUP: {
		if (wparam == VK_ESCAPE) {
			PostQuitMessage(0);
			return 0;
		}
	} break;
	case WM_CLOSE: {
		PostQuitMessage(0);
	} break;
	case WM_SIZE: {
		if (::onResize)
			(*::onResize)();
	} break;
	case WM_DESTROY: {
		if (::onDestroy)
			(*::onDestroy)();
	} break;
	case WM_ERASEBKGND: {
		ValidateRect(wnd, nullptr);
		return 1;
	} break;
	case WM_PAINT: {
		if (::onPaint) {
			(*::onPaint)();
		}
	} break;
	case WM_TIMER: {
		if (::onPaint) {
			(*::onPaint)();
		}
	} break;
	}
	return DefWindowProc(wnd, msg, wparam, lparam);
}

static std::wstring pathCombine(std::wstring path, std::wstring element) {
	std::vector<wchar_t> ret(1u<<16);
	PathCombine(std::data(ret), path.c_str(), element.c_str());
	return std::data(ret);
}

struct Node {
	float4x4 transform = float4x4::identity;
	Mesh mesh;
	bool isDrawable = false;

	std::vector<std::weak_ptr<Node>> children;
};

struct Drawable {
	float4x4 transform;
	Mesh mesh;
};

std::vector<Drawable> gatherDrawables(Node node, float4x4 parentTransform = float4x4::identity) {
	std::vector<Drawable> collection;
	float4x4 compositeTransform = parentTransform * node.transform;
	if (node.isDrawable) {
		collection.push_back({ compositeTransform, node.mesh });
	}
	for (auto& childPtr : node.children) {
		if (auto child = childPtr.lock()) {
			auto descendantDrawables = gatherDrawables(*child, compositeTransform);
			std::move(begin(descendantDrawables), end(descendantDrawables), std::back_inserter(collection));
		}
	}
	return collection;
}

struct FileMap {
	FileMap(std::wstring filename) {
		file = CreateFile(filename.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
		if (file == INVALID_HANDLE_VALUE) return;
		LARGE_INTEGER fileSize{};
		GetFileSizeEx(file, &fileSize);
		mapping = CreateFileMapping(file, nullptr, PAGE_READONLY, fileSize.HighPart, fileSize.LowPart, nullptr);
		if (mapping == nullptr) return;
		ptr = MapViewOfFile(mapping, FILE_MAP_READ, 0, 0, 0);
		length = fileSize.QuadPart;
	}

	~FileMap() {
		if (ptr != nullptr) UnmapViewOfFile(ptr);
		if (mapping != nullptr) CloseHandle(mapping);
		if (file != INVALID_HANDLE_VALUE) CloseHandle(file);
	}

	FileMap& operator = (FileMap const&) = delete;
	FileMap(FileMap const&) = delete;

	void* data() const { return ptr; }
	uint64_t size() const { return length; }

private:
	HANDLE file = INVALID_HANDLE_VALUE, mapping{};
	void* ptr{};
	uint64_t length{};
};

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE, LPSTR, int) {
	BreakingHResult hr{};
	WNDCLASSEX wcex{};
	wcex.cbSize = sizeof(wcex);
	wcex.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wcex.lpfnWndProc = &WindowProc;
	wcex.hInstance = hinstance;
	wcex.lpszClassName = L"ze-class";
	ATOM cls = RegisterClassEx(&wcex);
	HWND wnd = CreateWindow(reinterpret_cast<LPCWSTR>(cls), L"Standalone", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, 1280, 720, nullptr, nullptr, hinstance, 0);
	ShowWindow(wnd, SW_SHOW);

	std::vector<wchar_t> cwd(1u<<16);
	GetCurrentDirectory(static_cast<DWORD>(std::size(cwd)), std::data(cwd));
	std::wstring homedir = pathCombine(data(cwd), L"..\\..");
	std::wstring sitedir = pathCombine(data(cwd), L"" CMAKE_INTDIR);

	Graphics gfx(wnd);
	Renderer renderer(gfx.device, gfx.context, sitedir);

	float aspectRatio{};
	int clientWidth{1}, clientHeight{1};
	{
		RECT clientRect{};
		GetClientRect(wnd, &clientRect);
		aspectRatio = static_cast<float>(clientRect.right) / static_cast<float>(clientRect.bottom);
		clientWidth = clientRect.right;
		clientHeight = clientRect.bottom;
	}

	Frustum camera;
	camera.SetKind(FrustumSpaceD3D, FrustumLeftHanded);
	camera.SetVerticalFovAndAspectRatio(pi/2.0f, aspectRatio);
	camera.SetViewPlaneDistances(1.0f, 1000.0f);
	//camera.SetFrame(-100.0f * float3::unitZ, float3::unitZ, float3::unitY);
	float3 eye(0.0f, 40.0f, -100.0f);
	float3 at(0.0f, 0.0f, 0.0f);
	float3x4 cameraMatrix = float3x4::LookAt(eye, at, +float3::unitZ, +float3::unitY, +float3::unitY);
	camera.SetWorldMatrix(cameraMatrix);
	float4x4 viewMtx = camera.ViewMatrix();
	float4x4 projMtx = camera.ProjectionMatrix();

	Node root;
	std::vector<std::shared_ptr<Node>> cubes;
	std::vector<std::shared_ptr<Node>> rotators;
	std::vector<std::shared_ptr<Node>> translators;
	{
		Node templateCube{};
		templateCube.mesh = generateCube(gfx.device, float3(40.0f, 40.0f, 40.0f));
		templateCube.isDrawable = true;
		LCG lcg(9001u);
		for (size_t i = 0; i < 24; ++i) {
			auto cube = std::make_shared<Node>(templateCube);
			cubes.push_back(cube);

			auto rotator = std::make_shared<Node>();
			rotator->children.push_back(cube);
			rotators.push_back(rotator);

			auto translator = std::make_shared<Node>();
			translator->transform = static_cast<float4x4>(float4x4::Translate(float3::RandomSphere(lcg, float3::zero, 50.0f)));
			translator->children.push_back(rotator);
			translators.push_back(translator);
			
			root.children.push_back(translator);
		}
	}

	ID3D11DepthStencilViewPtr depthStencilDSV;
	D3D11_VIEWPORT viewport{};
	{
		D3D11_TEXTURE2D_DESC desc{};
		gfx.backBuffer->GetDesc(&desc);
		BreakingHResult hr{};
		ID3D11Texture2DPtr depthStencilBuffer;
		D3D11_TEXTURE2D_DESC texDesc{};
		texDesc.Width = clientWidth;
		texDesc.Height = clientHeight;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.Format = DXGI_FORMAT_D32_FLOAT;
		texDesc.SampleDesc = desc.SampleDesc;
		texDesc.Usage = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		texDesc.CPUAccessFlags = 0;
		texDesc.MiscFlags = 0;
		hr = gfx.device->CreateTexture2D(&texDesc, nullptr, &depthStencilBuffer);
		hr = gfx.device->CreateDepthStencilView(depthStencilBuffer, nullptr, &depthStencilDSV);

		viewport.Width = static_cast<float>(clientWidth);
		viewport.Height = static_cast<float>(clientHeight);
		viewport.MaxDepth = 1.0f;
	}
	ID3D11VertexShaderPtr untexturedMeshVS;
	ID3D11PixelShaderPtr untexturedMeshPS;
	{
		FileMap vsCSO(pathCombine(sitedir, L"UntexturedMeshVS.cso"));
		hr = gfx.device->CreateVertexShader(vsCSO.data(), vsCSO.size(), nullptr, &untexturedMeshVS);
		FileMap psCSO(pathCombine(sitedir, L"UntexturedMeshPS.cso"));
		hr = gfx.device->CreatePixelShader(psCSO.data(), psCSO.size(), nullptr, &untexturedMeshPS);
	}

	struct Instance {
		float4x4 modelViewProjection;
		float4x4 modelView;
	};

	LARGE_INTEGER baseTime{};
	QueryPerformanceCounter(&baseTime);
	LARGE_INTEGER nowTime{};
	LARGE_INTEGER timeFrequency{};
	QueryPerformanceFrequency(&timeFrequency);
	std::remove_reference_t<decltype(*::onPaint)> paint = [&](){
		QueryPerformanceCounter(&nowTime);
		float t = (nowTime.QuadPart - baseTime.QuadPart) / static_cast<float>(timeFrequency.QuadPart);
		std::for_each(begin(rotators), end(rotators), [t](auto node) {
			node->transform = float4x4::RotateY(t);
		});

		std::array<float, 4> clearColor{ 0.1f, 0.2f, 0.3f, 1.0f };
		gfx.context->ClearRenderTargetView(gfx.backBufferRTV, data(clearColor));
		gfx.context->ClearDepthStencilView(depthStencilDSV, D3D10_CLEAR_DEPTH, 1.0f, 0x00u);
		ID3D11RenderTargetView* omRTVs[] = { gfx.backBufferRTV };
		gfx.context->OMSetRenderTargets(1, omRTVs, depthStencilDSV);
		gfx.context->RSSetViewports(1, &viewport);

		auto drawables = gatherDrawables(root);

		for (auto& drawable : drawables) {
			ID3D11ShaderResourceViewPtr instanceSRV{};
			int instanceCount{};
			{
				instanceCount = 1;
				D3D11_BUFFER_DESC desc{};
				desc.ByteWidth = sizeof(Instance) * instanceCount;
				desc.Usage = D3D11_USAGE_IMMUTABLE;
				desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				desc.CPUAccessFlags = 0;
				desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
				desc.StructureByteStride = sizeof(Instance);
				float4x4 mv = viewMtx * drawable.transform;
				Instance instances[] = {
					{ (projMtx * mv).Transposed(), mv.Transposed() },
				};
				D3D11_SUBRESOURCE_DATA srd{};
				srd.pSysMem = instances;
				ID3D11BufferPtr instanceBuffer{};
				hr = gfx.device->CreateBuffer(&desc, &srd, &instanceBuffer);
				hr = gfx.device->CreateShaderResourceView(instanceBuffer, nullptr, &instanceSRV);
			}
			gfx.context->VSSetShader(untexturedMeshVS, nullptr, 0);
			gfx.context->PSSetShader(untexturedMeshPS, nullptr, 0);
			ID3D11ShaderResourceView* vsSRVs[] = { instanceSRV, drawable.mesh.vertexBufferSRV, drawable.mesh.indexBufferSRV };
			gfx.context->VSSetShaderResources(0, 3, vsSRVs);
			gfx.context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			gfx.context->DrawInstanced(drawable.mesh.indexCount, instanceCount, 0, 0);
		}
		gfx.swapChain->Present(0, 0);
	};
	::onPaint = &paint;

	SetTimer(wnd, 0x4242, 1000/121, nullptr);

	while (1) {
		MSG msg{};
		if (GetMessage(&msg, nullptr, 0, 0)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (msg.message == WM_QUIT) {
			DestroyWindow(wnd);
			break;
		}
	}
	return 0;
}