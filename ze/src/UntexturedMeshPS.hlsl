struct PS {
	float3 positionVS : ViewPosition;
	float2 texCoord : TexCoord;
	float3 tangentVS : Tangent;
	float3 bitangentVS : Bitangent;
	float3 normalVS : Normal;
	float4 position : SV_Position;
};

struct PointLight {
	float3 pos;
	float intensity;
	float pad0, pad1, pad2;
};

StructuredBuffer<PointLight> lights : register(t0);

float4 main(PS ps) : SV_Target {
	PointLight pl = {
		float3(0.0, 0.0, -1.0),
		2000.0, 0, 0, 0
	};
	float4 diffuse = float4(0.9, 0.8, 0.7, 1.0);
	float3 lVec = pl.pos - ps.positionVS.xyz;
	float lMag = length(lVec);
	float lDot = max(0.0, dot(lVec/lMag, ps.normalVS));
	float lInt = 1.0 - saturate(pow(lMag/pl.intensity, 0.5));
	float4 cv = lInt * lDot * diffuse;
	return cv;
}