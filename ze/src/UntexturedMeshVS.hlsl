struct VS {
	uint vertex : SV_VertexID;
	uint instance : SV_InstanceID;
};

struct PS {
	float3 positionVS : ViewPosition;
	float2 texCoord : TexCoord;
	float3 tangentVS : Tangent;
	float3 bitangentVS : Bitangent;
	float3 normalVS : Normal;
	float4 position : SV_Position;
};

struct Vertex {
	float3 position;
	float3 tangent;
	float3 bitangent;
	float3 normal;
	float2 texCoord;
};

struct Instance {
	float4x4 modelViewProjection;
	float4x4 modelView;
};

StructuredBuffer<Instance> instances : register(t0);
StructuredBuffer<Vertex> vertices : register(t1);
StructuredBuffer<uint> indices : register(t2);

PS main(VS vs) {
	Instance instance = instances[vs.instance];
	Vertex vtx = vertices[indices[vs.vertex]];
	float4 pos = float4(vtx.position, 1.0);
	PS ps;
	ps.positionVS = mul(instance.modelView, pos).xyz;
	ps.texCoord = vtx.texCoord;
	float3x3 normalMatrix = (float3x3)instance.modelView;
	ps.tangentVS = mul(normalMatrix, vtx.tangent);
	ps.bitangentVS = mul(normalMatrix, vtx.bitangent);
	ps.normalVS = mul(normalMatrix, vtx.normal);
	ps.position = mul(instance.modelViewProjection, pos);
	return ps;
}