#pragma once 

#include <comdef.h>
#include <d3d11.h>
#include <dxgi.h>

#define DECLARE_COM_SMARTPTR(Name) _COM_SMARTPTR_TYPEDEF(Name, __uuidof(Name))

DECLARE_COM_SMARTPTR(ID3D11BlendState);
DECLARE_COM_SMARTPTR(ID3D11Buffer);
DECLARE_COM_SMARTPTR(ID3D11DepthStencilState);
DECLARE_COM_SMARTPTR(ID3D11DepthStencilView);
DECLARE_COM_SMARTPTR(ID3D11Device);
DECLARE_COM_SMARTPTR(ID3D11DeviceContext);
DECLARE_COM_SMARTPTR(ID3D11PixelShader);
DECLARE_COM_SMARTPTR(ID3D11RenderTargetView);
DECLARE_COM_SMARTPTR(ID3D11ShaderResourceView);
DECLARE_COM_SMARTPTR(ID3D11Texture2D);
DECLARE_COM_SMARTPTR(ID3D11VertexShader);
DECLARE_COM_SMARTPTR(IDXGIAdapter);
DECLARE_COM_SMARTPTR(IDXGIDevice);
DECLARE_COM_SMARTPTR(IDXGIFactory);
DECLARE_COM_SMARTPTR(IDXGISwapChain);

struct BreakingHResult {
	BreakingHResult(HRESULT hr = S_OK) {
		assign(hr);
	}

	BreakingHResult& operator = (HRESULT hr) {
		assign(hr);
		return *this;
	}

	void assign(HRESULT hr) {
		this->hr = hr;
		if (FAILED(hr)) {
			DebugBreak();
		}
	}

	HRESULT hr;
};