#pragma once

#include <stdint.h>

#include <MathGeoLib/MathGeoLib.h>

#include "DirectXTypes.h"

struct Mesh {
	uint32_t vertexCount{}, indexCount{};
	ID3D11BufferPtr vertexBuffer, indexBuffer;
	ID3D11ShaderResourceViewPtr vertexBufferSRV, indexBufferSRV;
};

Mesh generateCube(ID3D11Device* device, float3 extents);